package com.axot.dziennikpiwowara.database.entitis

data class BeerMinimal(
    var id: Long = 0,
    var name: String = "",
    var style: String = "",
    var blg: Double = 0.0
)