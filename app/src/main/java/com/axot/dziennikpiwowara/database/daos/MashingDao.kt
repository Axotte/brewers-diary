package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.dziennikpiwowara.database.entitis.Mashing

@Dao
interface MashingDao: BaseDao<Mashing> {
    @Query("SELECT * FROM mashing WHERE beerId = :beerId")
    fun getAllMashing(beerId: Long): LiveData<List<Mashing>>

    @Query("SELECT * FROM mashing WHERE id = :id")
    fun getMashingById(id: Long): LiveData<Mashing>
}