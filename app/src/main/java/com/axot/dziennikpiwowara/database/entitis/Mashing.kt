package com.axot.dziennikpiwowara.database.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "mashing",
    foreignKeys = [ForeignKey(entity = Beer::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("beerId"),
        onDelete = ForeignKey.CASCADE)])
data class Mashing (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var beerId: Long = 0,
    var temperature: Int = 0,
    var time: Int = 0
)