package com.axot.dziennikpiwowara.database.entitis

import android.arch.persistence.room.*

@Entity(tableName = "beers")
data class Beer(
    var name: String = "",
    var style: String = "",
    var blg: Double = 0.0,
    var yeast: String = "",
    var alkohol: Double = 0.0,
    var date: Long = 0,
    var ibu: Int = 0,
    var cookingTime: Int = 0,
    var size: Int = 0,
    var comments: String = "",
    @Embedded
    var bottling: Bottling = Bottling(),
    @Embedded
    var fermentation: Fermentation = Fermentation(),
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var valible: Boolean = false
)