package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.dziennikpiwowara.database.entitis.FermAdditive

@Dao
interface FermAdditivesDao: BaseDao<FermAdditive> {
    @Query("SELECT * FROM ferm_additives WHERE beerId = :beerId")
    fun getAllFermAdditives(beerId: Long): LiveData<List<FermAdditive>>

    @Query("SELECT * FROM ferm_additives WHERE id = :id")
    fun getFermAdditiveById(id: Long): LiveData<FermAdditive>
}