package com.axot.dziennikpiwowara.database.entitis

data class Bottling (
    var bottlingDate: Long = 0,
    var co2Lvl: Double = 0.0,
    var refermentationMeterial: String = "",
    var amountOfMaterial: Int = 0,
    var but033: Int = 0,
    var but05: Int = 0
)