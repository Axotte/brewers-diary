package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.dziennikpiwowara.database.entitis.Charge

@Dao
interface ChargeDao: BaseDao<Charge> {
    @Query("SELECT * FROM charge WHERE beerId = :beerId")
    fun getAllCharge(beerId: Long): LiveData<List<Charge>>

    @Query("SELECT * FROM charge WHERE id = :id")
    fun getChargeById(id: Long): LiveData<Charge>

}