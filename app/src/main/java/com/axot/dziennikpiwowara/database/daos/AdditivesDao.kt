package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.dziennikpiwowara.database.entitis.Additive

@Dao
interface AdditivesDao: BaseDao<Additive> {
    @Query("SELECT * FROM additives WHERE beerId = :beerId")
    fun getAllAdditives(beerId: Long): LiveData<List<Additive>>

    @Query("SELECT * FROM additives WHERE id = :id")
    fun getAdditiveById(id: Long): LiveData<Additive>

}