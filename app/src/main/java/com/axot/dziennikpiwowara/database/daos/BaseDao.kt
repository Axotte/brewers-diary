package com.axot.dziennikpiwowara.database.daos

import android.arch.persistence.room.*

@Dao
interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(type: T): Long

    @Delete
    fun delete(type: T)

    @Update
    fun update(type: T)
}