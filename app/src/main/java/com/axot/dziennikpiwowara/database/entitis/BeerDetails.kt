package com.axot.dziennikpiwowara.database.entitis

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class BeerDetails {

    @Embedded
    var beer: Beer ?= null

    @Relation(parentColumn = "id", entityColumn = "beerId")
    var additives: List<Additive> = ArrayList()

    @Relation(parentColumn = "id", entityColumn = "beerId")
    var charge: List<Charge> = ArrayList()

    @Relation(parentColumn = "id", entityColumn = "beerId")
    var hops: List<Hop> = ArrayList()

    @Relation(parentColumn = "id", entityColumn = "beerId")
    var mashing: List<Mashing> = ArrayList()

    @Relation(parentColumn = "id", entityColumn = "beerId")
    var fermAdditives: List<FermAdditive> = ArrayList()
}