package com.axot.dziennikpiwowara.database.entitis

data class Fermentation(
    var startDate: Long = 0,
    var finishDate: Long = 0,
    var finishBlg: Double = 0.0,
    var temperature: Int = 0,
    var yeastTemperature: Int = 0
)