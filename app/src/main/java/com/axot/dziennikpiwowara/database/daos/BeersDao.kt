package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.axot.dziennikpiwowara.database.entitis.Beer
import com.axot.dziennikpiwowara.database.entitis.BeerDetails
import com.axot.dziennikpiwowara.database.entitis.BeerMinimal

@Dao
interface BeersDao : BaseDao<Beer> {

    @Query("SELECT * FROM beers")
    fun getAllBeers(): LiveData<List<Beer>>

    @Query("SELECT id, name, style, blg FROM beers")
    fun getAllBeersMinimal(): LiveData<List<BeerMinimal>>

    @Query("SELECT * FROM beers WHERE id = :id")
    fun getBeerById(id: Long): LiveData<Beer>

    @Query("DELETE FROM beers")
    fun deleteAllBeers()

    @Transaction
    @Query("SELECT * FROM beers WHERE id = :id")
    fun getBeerDetailsById(id: Long): LiveData<BeerDetails>

    @Query("SELECT * FROM beers WHERE valible = 0")
    fun getBeersToDelete(): LiveData<List<Beer>>
}