package com.axot.dziennikpiwowara.database.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "hops",
    foreignKeys = [ForeignKey(entity = Beer::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("beerId"),
        onDelete = ForeignKey.CASCADE)])
data class Hop (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var beerId: Long = 0,
    var name: String = "",
    var amount: Int = 0,
    var time: Int = 0,
    var alfa: Double = 0.0
)