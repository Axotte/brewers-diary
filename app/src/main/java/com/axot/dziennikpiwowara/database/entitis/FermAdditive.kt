package com.axot.dziennikpiwowara.database.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "ferm_additives",
    foreignKeys = [ForeignKey(entity = Beer::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("beerId"),
        onDelete = ForeignKey.CASCADE)])
data class FermAdditive (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var beerId: Long = 0,
    var name: String = "",
    var amount: Int = 0,
    var time: Int = 0
)