package com.axot.dziennikpiwowara.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.dziennikpiwowara.database.entitis.Hop

@Dao
interface HopsDao: BaseDao<Hop> {
    @Query("SELECT * FROM hops WHERE beerId = :beerId")
    fun getAllHops(beerId: Long): LiveData<List<Hop>>

    @Query("SELECT * FROM hops WHERE id = :id")
    fun getHopById(id: Long): LiveData<Hop>
}