package com.axot.dziennikpiwowara.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.axot.dziennikpiwowara.database.daos.*
import com.axot.dziennikpiwowara.database.entitis.*

@Database(entities = [(Beer::class), (Additive::class), (Charge::class), (Hop::class), (Mashing::class), (FermAdditive::class)],
    version = 4)
abstract class BeersDatabase : RoomDatabase() {

    abstract fun beersDao(): BeersDao
    abstract fun additivesDao(): AdditivesDao
    abstract fun chargeDao(): ChargeDao
    abstract fun hopsDao(): HopsDao
    abstract fun mashingDao(): MashingDao
    abstract fun fermAdditivesDao(): FermAdditivesDao

    companion object {
        @Volatile
        private var INSTANCE: BeersDatabase? = null

        fun getDatabase(context: Context): BeersDatabase {
            val tempInstance = INSTANCE
            if(tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, BeersDatabase::class.java, "BeersDB").fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}