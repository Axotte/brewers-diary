package com.axot.dziennikpiwowara.view.mainview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.view.assistant.Assistant
import com.axot.dziennikpiwowara.view.calculator.Calculator
import com.axot.dziennikpiwowara.view.diary.Container

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager: ViewPager = findViewById(R.id.pager)
        viewPager.offscreenPageLimit = 2
        val adapter = PagerAdapter(supportFragmentManager)
        adapter.addFragment(Container(),getString(R.string.diary))
        adapter.addFragment(Calculator(),getString(R.string.calculator))
        adapter.addFragment(Assistant(),getString(R.string.assistant))
        viewPager.adapter = adapter

        val tabLayout: TabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)
    }
}
