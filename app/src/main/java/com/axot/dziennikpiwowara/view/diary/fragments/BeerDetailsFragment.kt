package com.axot.dziennikpiwowara.view.diary.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Beer
import com.axot.dziennikpiwowara.utils.BeerValidator
import com.axot.dziennikpiwowara.view.diary.adapters.*
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel
import org.jetbrains.anko.support.v4.alert

class BeerDetailsFragment : Fragment() {

    private lateinit var beersViewModel: BeersViewModel
    var id: Long ?= null

    val validator = BeerValidator()
    private lateinit var chargeRvAdapter: ChargeRecyclerViewAdapter
    private lateinit var mashingRvAdapter: MashingRecyclerViewAdapter
    private lateinit var hopsRvAdapter: HopsRecyclerViewAdapter
    private lateinit var additivesRvAdapter: AdditivesRecyclerViewAdapter
    private lateinit var fermAdditivesRvAdapter: FermAdditivesRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_beer_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)

        insertDetails()
        setUpRecyclerViews()

        beersViewModel.getAllCharge(id!!).observe(this, Observer {charge ->
            charge?.let {
                chargeRvAdapter.setCharge(it)
            }
        })

        beersViewModel.getAllMashing(id!!).observe(this, Observer {mashing ->
            mashing?.let {
                mashingRvAdapter.setMashing(it)
            }
        })

        beersViewModel.getAllHops(id!!).observe(this, Observer { hops ->
            hops?.let {
                hopsRvAdapter.setHops(it)
            }
        })

        beersViewModel.getAllAdditives(id!!).observe(this, Observer { additives ->
            additives?.let {
                additivesRvAdapter.setAdditives(it)
            }
        })

        beersViewModel.getAllFermAdditives(id!!).observe(this, Observer { fermAdditives ->
            fermAdditives?.let {
                fermAdditivesRvAdapter.setFermAdditives(it)
            }
        })

        view!!.findViewById<FloatingActionButton>(R.id.fab_edit).setOnClickListener {
            val addEditBeerFragment = AddEditBeerFragment()
            addEditBeerFragment.id = id
            addEditBeerFragment.worthToSave = true
            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, addEditBeerFragment)
                .addToBackStack("fromDetailsToAddEdit").commit()
        }

        view!!.findViewById<FloatingActionButton>(R.id.fab_delete_beer).setOnClickListener {
            alert {
                title = "Na pewno chcesz usunąć?"
                positiveButton("Tak") {
                    beersViewModel.deleteBeer(Beer(id = id!!))
                    fragmentManager!!.popBackStackImmediate()
                }
                negativeButton("Nie") {}
            }.show()


        }
    }

    private fun setUpRecyclerViews() {
        val rvCharge = view!!.findViewById<RecyclerView>(R.id.rv_charge_details)
        val rvMashig = view!!.findViewById<RecyclerView>(R.id.rv_mashing_details)
        val rvHops = view!!.findViewById<RecyclerView>(R.id.rv_hops_details)
        val rvAdditives = view!!.findViewById<RecyclerView>(R.id.rv_additives_details)
        val rvFermAdditives = view!!.findViewById<RecyclerView>(R.id.rv_ferm_additives_details)

        chargeRvAdapter = ChargeRecyclerViewAdapter(activity!!)
        mashingRvAdapter = MashingRecyclerViewAdapter(activity!!)
        hopsRvAdapter = HopsRecyclerViewAdapter(activity!!)
        additivesRvAdapter = AdditivesRecyclerViewAdapter(activity!!)
        fermAdditivesRvAdapter = FermAdditivesRecyclerViewAdapter(activity!!)

        rvCharge.adapter = chargeRvAdapter
        rvMashig.adapter = mashingRvAdapter
        rvHops.adapter = hopsRvAdapter
        rvAdditives.adapter = additivesRvAdapter
        rvFermAdditives.adapter = fermAdditivesRvAdapter

        rvCharge.layoutManager = LinearLayoutManager(activity!!)
        rvMashig.layoutManager = LinearLayoutManager(activity!!)
        rvHops.layoutManager = LinearLayoutManager(activity!!)
        rvAdditives.layoutManager = LinearLayoutManager(activity!!)
        rvFermAdditives.layoutManager = LinearLayoutManager(activity!!)
    }

    private fun insertDetails() {
        beersViewModel.getBeerDetalisById(id!!).observe(this, Observer {beer ->
            beer?.let {
                view!!.findViewById<TextView>(R.id.tv_name_details).text = validator.toTextView(it.beer!!.name)
                view!!.findViewById<TextView>(R.id.tv_style_details).text = validator.toTextView(it.beer!!.style)
                view!!.findViewById<TextView>(R.id.tv_blg_details).text = validator.toTextViewBlg(it.beer!!.blg)
                view!!.findViewById<TextView>(R.id.tv_alk_details).text = validator.toTextViewPercent(it.beer!!.alkohol)
                view!!.findViewById<TextView>(R.id.tv_ibu_details).text = validator.toTextView(it.beer!!.ibu)
                view!!.findViewById<TextView>(R.id.tv_size_details).text = validator.toTextViewL(it.beer!!.size)
                view!!.findViewById<TextView>(R.id.tv_date_details).text = validator.longDateToString(it.beer!!.date)
                view!!.findViewById<TextView>(R.id.tv_brewing_time_details).text = validator.toTextViewMinutes(it.beer!!.cookingTime)
                view!!.findViewById<TextView>(R.id.tv_start_date_details).text = validator
                    .longDateToString(it.beer!!.fermentation.startDate)
                view!!.findViewById<TextView>(R.id.tv_stop_date_details).text = validator
                    .longDateToString(it.beer!!.fermentation.finishDate)
                view!!.findViewById<TextView>(R.id.tv_ferm_temp_details).text = validator
                    .toTextViewCelcius(it.beer!!.fermentation.temperature)
                view!!.findViewById<TextView>(R.id.tv_yeast_temp_details).text = validator
                    .toTextViewCelcius(it.beer!!.fermentation.yeastTemperature)
                view!!.findViewById<TextView>(R.id.tv_finish_blg_details).text = validator
                    .toTextViewBlg(it.beer!!.fermentation.finishBlg)
                view!!.findViewById<TextView>(R.id.tv_bottling_date_details).text = validator
                    .longDateToString(it.beer!!.bottling.bottlingDate)
                view!!.findViewById<TextView>(R.id.tv_co2_details).text = validator
                    .toTextView(it.beer!!.bottling.co2Lvl)
                view!!.findViewById<TextView>(R.id.tv_referm_name).text = validator
                    .toTextView(it.beer!!.bottling.refermentationMeterial)
                view!!.findViewById<TextView>(R.id.tv_referm_amount).text = validator
                    .toTextViewG(it.beer!!.bottling.amountOfMaterial)
                view!!.findViewById<TextView>(R.id.tv_bottle330_amount).text = validator
                    .toTextView(it.beer!!.bottling.but033)
                view!!.findViewById<TextView>(R.id.tv_bottle500_amount).text = validator
                    .toTextView(it.beer!!.bottling.but05)
                view!!.findViewById<TextView>(R.id.tv_comments_details).text = it.beer!!.comments
            }
        })
    }
}
