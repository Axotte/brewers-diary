package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Hop

class HopsRecyclerViewAdapter(context: Context):
RecyclerView.Adapter<HopsRecyclerViewAdapter.HopViewHolder>() {

    private var hops: List<Hop> = emptyList()
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HopViewHolder {
        val itemView = inflater.inflate(R.layout.hop_rv_item, parent, false)
        return HopViewHolder(itemView)
    }

    override fun getItemCount(): Int = hops.size

    override fun onBindViewHolder(holder: HopViewHolder, position: Int) {
        val name = "Nazwa: ${hops[position].name}"
        val alfa = "Alfa:\n ${hops[position].alfa}"
        val amount = "Ilość:\n ${hops[position].amount} g"
        val time = "Czas:\n ${hops[position].time} min"

        holder.hopName.text = name
        holder.hopAlfa.text = alfa
        holder.hopAmount.text = amount
        holder.hopTime.text = time
    }

    fun setHops(hops: List<Hop>) {
        this.hops = hops
        notifyDataSetChanged()
    }

    fun getHopAt(position: Int): Hop = hops[position]

    inner class HopViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val hopName = itemView.findViewById<TextView>(R.id.tv_hop_name)!!
        val hopAlfa = itemView.findViewById<TextView>(R.id.tv_hop_alfa)!!
        val hopAmount = itemView.findViewById<TextView>(R.id.tv_hop_amount)!!
        val hopTime = itemView.findViewById<TextView>(R.id.tv_hop_time)!!
    }
}