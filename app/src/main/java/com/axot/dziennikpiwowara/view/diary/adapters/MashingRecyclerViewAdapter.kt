package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Mashing

class MashingRecyclerViewAdapter internal constructor(context: Context):
    RecyclerView.Adapter<MashingRecyclerViewAdapter.MashingViewHolder>(){

    private var mashing: List<Mashing> = emptyList()
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MashingViewHolder {
        val itemView = inflater.inflate(R.layout.mashing_rv_item, parent, false)
        return MashingViewHolder(itemView)
    }

    override fun getItemCount(): Int = mashing.size

    override fun onBindViewHolder(holder: MashingViewHolder, position: Int) {
        val temperature = "Temperatura: ${mashing[position].temperature} °C"
        val time = "Długość przerwy: ${mashing[position].time} min"
        holder.mashingTempereture.text = temperature
        holder.mashingTime.text = time
    }

    fun setMashing(mashing: List<Mashing>) {
        this.mashing = mashing
        notifyDataSetChanged()
    }

    fun getMashingAt(position: Int): Mashing = mashing[position]

    inner class MashingViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val mashingTempereture = itemView.findViewById<TextView>(R.id.tv_mashing_temperature)!!
        val mashingTime = itemView.findViewById<TextView>(R.id.tv_mashing_time)!!
    }
}