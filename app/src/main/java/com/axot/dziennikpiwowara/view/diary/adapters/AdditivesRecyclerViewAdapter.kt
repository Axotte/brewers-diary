package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Additive

class AdditivesRecyclerViewAdapter(context: Context):
RecyclerView.Adapter<AdditivesRecyclerViewAdapter.AdditivesViewHolder>() {

    private var additives: List<Additive> = emptyList()
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdditivesViewHolder {
        val itemView = inflater.inflate(R.layout.additive_rv_item, parent, false)
        return AdditivesViewHolder(itemView)
    }

    override fun getItemCount(): Int = additives.size

    override fun onBindViewHolder(holder: AdditivesViewHolder, position: Int) {

        val name = "Nazwa: ${additives[position].name}"
        val amount = "Ilość: ${additives[position].amount} g"
        val time = "Czas: ${additives[position].time} min"

        holder.additiveName.text = name
        holder.additiveAmount.text = amount
        holder.additiveTime.text = time
    }

    fun setAdditives(additives: List<Additive>) {
        this.additives = additives
        notifyDataSetChanged()
    }

    fun getAdditiveAt(position: Int): Additive = additives[position]

    inner class AdditivesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val additiveName = itemView.findViewById<TextView>(R.id.tv_additive_name)!!
        val additiveAmount = itemView.findViewById<TextView>(R.id.tv_additive_amount)!!
        val additiveTime = itemView.findViewById<TextView>(R.id.tv_additive_time)!!
    }
}