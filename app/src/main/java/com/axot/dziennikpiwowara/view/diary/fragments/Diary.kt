package com.axot.dziennikpiwowara.view.diary.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.BeerMinimal
import com.axot.dziennikpiwowara.view.diary.adapters.BeersRecyclerViewAdapter
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel


class Diary : Fragment() {

    private lateinit var beersViewModel: BeersViewModel
    private lateinit var adapter: BeersRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_diary, container, false)
        adapter = BeersRecyclerViewAdapter(activity!!)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity!!)

        val addBeerFab: FloatingActionButton = view.findViewById(R.id.fab_add_beer)
        addBeerFab.setOnClickListener{
            fragmentManager!!.beginTransaction().replace(R.id.fragment_container,
                AddEditBeerFragment()
            ).addToBackStack("add_edit").commit()
        }

        adapter.listener = object: BeersRecyclerViewAdapter.OnItemClickListener {
            override fun onItemClick(beer: BeerMinimal) {
                val details = BeerDetailsFragment()
                details.id = beer.id
                fragmentManager!!.beginTransaction().replace(R.id.fragment_container, details).addToBackStack("edit").commit()
            }
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)

        beersViewModel.beersToDelete.observe(this, Observer { beers ->
            beers?.let {
                for (i in beers) {
                    beersViewModel.deleteBeer(i)
                }
            }
        })
        beersViewModel.allBeersMinimal.observe(this, Observer { beers ->
            beers?.let { adapter.setBeers(it) }
        })
    }
}
