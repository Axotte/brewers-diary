package com.axot.dziennikpiwowara.view.diary


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.view.diary.fragments.Diary

class Container : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_container, container, false)

        fragmentManager!!.beginTransaction().replace(R.id.fragment_container, Diary()).commit()
        return view
    }


}
