package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Charge

class ChargeRecyclerViewAdapter(context: Context):
    RecyclerView.Adapter<ChargeRecyclerViewAdapter.ChargeViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private var charge: List<Charge> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChargeViewHolder {
        val itemView = inflater.inflate(R.layout.charge_rv_item, parent, false)
        return ChargeViewHolder(itemView)
    }

    override fun getItemCount(): Int = charge.size

    override fun onBindViewHolder(holder: ChargeViewHolder, position: Int) {
        val amount = "${charge[position].amountOfMalt} kg"
        holder.chargeName.text = charge[position].malt
        holder.chargeAmount.text = amount
    }

    fun setCharge(charge: List<Charge>) {
        this.charge = charge
        notifyDataSetChanged()
    }

    fun getChargeAt(position: Int): Charge = charge[position]

    inner class ChargeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val chargeName = itemView.findViewById<TextView>(R.id.tv_charge_name)!!
        val chargeAmount = itemView.findViewById<TextView>(R.id.tv_charge_amount)!!
    }
}