package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.FermAdditive

class FermAdditivesRecyclerViewAdapter(context: Context):
RecyclerView.Adapter<FermAdditivesRecyclerViewAdapter.FermAdditivesViewHolder>(){

    private var fermAdditives: List<FermAdditive> = emptyList()
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FermAdditivesViewHolder {
        val itemView = inflater.inflate(R.layout.ferm_additive_rv_item, parent, false)
        return FermAdditivesViewHolder(itemView)
    }

    override fun getItemCount(): Int = fermAdditives.size

    override fun onBindViewHolder(holder: FermAdditivesViewHolder, position: Int) {
        val name = "Nazwa: ${fermAdditives[position].name}"
        val amount = "Ilość: ${fermAdditives[position].amount} g"
        val time = "${fermAdditives[position].time} dzień"
        holder.fermAdditiveName.text = name
        holder.fermAdditivesAmount.text = amount
        holder.fermAdditivesTime.text = time
    }

    fun setFermAdditives(fermAdditives: List<FermAdditive>) {
        this.fermAdditives = fermAdditives
        notifyDataSetChanged()
    }

    fun getFermAdditivesAt(position: Int): FermAdditive = fermAdditives[position]

    inner class FermAdditivesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val fermAdditiveName = itemView.findViewById<TextView>(R.id.tv_ferm_additive_name)!!
        val fermAdditivesAmount = itemView.findViewById<TextView>(R.id.tv_ferm_additive_amount)!!
        val fermAdditivesTime = itemView.findViewById<TextView>(R.id.tv_ferm_additive_time)!!
    }
}