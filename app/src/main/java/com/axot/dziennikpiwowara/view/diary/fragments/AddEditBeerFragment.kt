package com.axot.dziennikpiwowara.view.diary.fragments


import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Beer
import com.axot.dziennikpiwowara.database.entitis.Bottling
import com.axot.dziennikpiwowara.database.entitis.Fermentation
import com.axot.dziennikpiwowara.utils.BeerValidator
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel
import java.util.*


class AddEditBeerFragment : Fragment() {

    var id: Long? = null
    var mainDate: Long? = null
    var worthToSave: Boolean = false
    var startFermDate: Long? = null
    var finishFermDate: Long? = null
    var bottlingDate: Long? = null

    private lateinit var validator: BeerValidator

    private lateinit var beersViewModel: BeersViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_beer, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        validator = BeerValidator()
        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)

        if(id == null) {
            beersViewModel.insertBeer(Beer())
            beersViewModel.getId().observe(this, Observer { beerId ->
                id = beerId!!
            })
        } else {
            worthToSave = true
            insertCurrentInformation(id!!)
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Listeners
        view!!.findViewById<FloatingActionButton>(R.id.save_fab).setOnClickListener {
            if (id == null) {
                Toast.makeText(activity!!, "Coś poszło nie tak", Toast.LENGTH_LONG).show()
            } else {
                updateBeerObject(true)
                fragmentManager!!.popBackStackImmediate()
            }
        }

        view!!.findViewById<Button>(R.id.but_charge_mashing).setOnClickListener {
            if (id == null) {
                Toast.makeText(activity!!, "Coś poszło nie tak", Toast.LENGTH_LONG).show()
            } else {
                updateBeerObject(worthToSave)
                val addEditChargeMashing = AddEditChargeMashing()
                addEditChargeMashing.currentBeerId = id
                fragmentManager!!.beginTransaction().replace(R.id.fragment_container, addEditChargeMashing)
                    .addToBackStack("addEditChargeMashing").commit()
            }
        }

        view!!.findViewById<Button>(R.id.but_hop_additive).setOnClickListener {
            if (id == null) {
                Toast.makeText(activity!!, "Coś poszło nie tak", Toast.LENGTH_LONG).show()
            } else {
                updateBeerObject(worthToSave)
                val addEditHopsAdditives = AddEditHopsAdditives()
                addEditHopsAdditives.currentBeerId = id
                fragmentManager!!.beginTransaction().replace(R.id.fragment_container, addEditHopsAdditives)
                    .addToBackStack("addEditHopsAdditives").commit()
            }
        }

        view!!.findViewById<Button>(R.id.but_ferm_additives).setOnClickListener {
            if (id == null) {
                Toast.makeText(activity!!, "Coś poszło nie tak", Toast.LENGTH_LONG).show()
            } else {
                updateBeerObject(worthToSave)
                val addEditFermAdditives = AddEditFermAdditives()
                addEditFermAdditives.currentBeerId = id
                fragmentManager!!.beginTransaction().replace(R.id.fragment_container, addEditFermAdditives)
                    .addToBackStack("addEditFermAdditives").commit()
            }
        }


        view!!.findViewById<Button>(R.id.but_main_date).setOnClickListener {
            val calendar = Calendar.getInstance()
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val m = calendar.get(Calendar.MONTH)
            val y = calendar.get(Calendar.YEAR)
            val tvMainDate = view!!.findViewById<TextView>(R.id.tv_main_date)

            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                mainDate = validator.dateToLong(year, month, dayOfMonth)
                val date = "$dayOfMonth/$month/$year"
                tvMainDate.text = date
            }, y, m, d)
            datePickerDialog.show()
        }

        view!!.findViewById<Button>(R.id.but_start_ferm).setOnClickListener {
            val calendar = Calendar.getInstance()
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val m = calendar.get(Calendar.MONTH)
            val y = calendar.get(Calendar.YEAR)
            val startFerm = view!!.findViewById<TextView>(R.id.tv_start_ferm)

            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                startFermDate = validator.dateToLong(year, month, dayOfMonth)
                val date = "$dayOfMonth/$month/$year"
                startFerm.text = date
            }, y, m, d)
            datePickerDialog.show()
        }

        view!!.findViewById<Button>(R.id.but_stop_ferm).setOnClickListener {
            val calendar = Calendar.getInstance()
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val m = calendar.get(Calendar.MONTH)
            val y = calendar.get(Calendar.YEAR)
            val stopFerm = view!!.findViewById<TextView>(R.id.tv_stop_ferm)

            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                finishFermDate = validator.dateToLong(year, month, dayOfMonth)
                val date = "$dayOfMonth/$month/$year"
                stopFerm.text = date
            }, y, m, d)
            datePickerDialog.show()
        }

        view!!.findViewById<Button>(R.id.but_bottling_date).setOnClickListener {
            val calendar = Calendar.getInstance()
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val m = calendar.get(Calendar.MONTH)
            val y = calendar.get(Calendar.YEAR)
            val tvBottlingDate = view!!.findViewById<TextView>(R.id.tv_bottling_date)

            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                bottlingDate = validator.dateToLong(year, month, dayOfMonth)
                val date = "$dayOfMonth/$month/$year"
                tvBottlingDate.text = date
            }, y, m, d)
            datePickerDialog.show()
        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    private fun updateBeerObject(valible: Boolean) {
        val name: String = view!!.findViewById<EditText>(R.id.et_name).text.toString()
        val style: String = view!!.findViewById<EditText>(R.id.et_style).text.toString()
        val blg: Double = validator.toDoubleOrDefault(view!!.findViewById<EditText>(R.id.et_blg).text.toString())
        val yeast: String = view!!.findViewById<EditText>(R.id.et_yeast).text.toString()
        val alk: Double = validator.toDoubleOrDefault(view!!.findViewById<EditText>(R.id.et_alk).text.toString())
        val size: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_size).text.toString())
        val ibu: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_ibu).text.toString())
        val brewingTime: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_time).text.toString())
        val comments:String = view!!.findViewById<EditText>(R.id.et_comments).text.toString()
        val dateMain: Long = if(mainDate == null) 0 else mainDate!!

        val yeastTemp: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_yeast_temperature).text.toString())
        val fermTemp: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_ferm_temperature).text.toString())
        val finishBlg: Double = validator.toDoubleOrDefault(view!!.findViewById<EditText>(R.id.et_finish_blg).text.toString())
        val dateStartFerm: Long = if(startFermDate == null) 0 else startFermDate!!
        val dateFinishFerm: Long = if(finishFermDate == null) 0 else finishFermDate!!
        val fermentation = Fermentation(dateStartFerm, dateFinishFerm, finishBlg, fermTemp, yeastTemp)

        val dateBottling = if(bottlingDate == null) 0 else bottlingDate!!
        val co2lvl: Double = validator.toDoubleOrDefault(view!!.findViewById<EditText>(R.id.et_co2_lvl).text.toString())
        val refermName: String = view!!.findViewById<EditText>(R.id.et_referm_name).text.toString()
        val refermAmount: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_referm_amout).text.toString())
        val but330: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_but330).text.toString())
        val but500: Int = validator.toIntOrDefault(view!!.findViewById<EditText>(R.id.et_but500).text.toString())
        val bottling = Bottling(dateBottling, co2lvl, refermName, refermAmount, but330, but500)

        val b = Beer(name, style, blg, yeast, alk, dateMain, ibu,
            brewingTime, size, comments, bottling, fermentation, id!!)

            beersViewModel.updateBeer(
                Beer(name, style, blg, yeast, alk, dateMain, ibu,
                    brewingTime, size, comments, bottling, fermentation, id!!, valible))
    }


    fun insertCurrentInformation(id: Long) {
        beersViewModel.getBeerById(id).observe(this, Observer { beer ->
            beer?.let {
                view!!.findViewById<EditText>(R.id.et_name).setText(it.name)
                view!!.findViewById<EditText>(R.id.et_style).setText(it.style)
                view!!.findViewById<EditText>(R.id.et_blg).setText(validator.toStringOrEmpty(it.blg))
                view!!.findViewById<EditText>(R.id.et_yeast).setText(it.yeast)
                view!!.findViewById<EditText>(R.id.et_alk).setText(validator.toStringOrEmpty(it.alkohol))
                view!!.findViewById<EditText>(R.id.et_size).setText(validator.toStringOrEmpty(it.size))
                view!!.findViewById<EditText>(R.id.et_ibu).setText(validator.toStringOrEmpty(it.ibu))
                view!!.findViewById<EditText>(R.id.et_time).setText(validator.toStringOrEmpty(it.cookingTime))
                view!!.findViewById<EditText>(R.id.et_comments).setText(it.comments)
                mainDate = it.date
                view!!.findViewById<TextView>(R.id.tv_main_date).text = validator.longDateToString(it.date)

                view!!.findViewById<EditText>(R.id.et_yeast_temperature)
                    .setText(validator.toStringOrEmpty(it.fermentation.yeastTemperature))
                view!!.findViewById<EditText>(R.id.et_ferm_temperature)
                    .setText(validator.toStringOrEmpty(it.fermentation.temperature))
                view!!.findViewById<EditText>(R.id.et_finish_blg).setText(validator.toStringOrEmpty(it.fermentation.finishBlg))
                startFermDate = it.fermentation.startDate
                finishFermDate = it.fermentation.finishDate
                view!!.findViewById<TextView>(R.id.tv_start_ferm).text = validator.longDateToString(it.fermentation.startDate)
                view!!.findViewById<TextView>(R.id.tv_stop_ferm).text = validator.longDateToString(it.fermentation.finishDate)

                view!!.findViewById<EditText>(R.id.et_co2_lvl).setText(validator.toStringOrEmpty(it.bottling.co2Lvl))
                view!!.findViewById<EditText>(R.id.et_referm_name).setText(it.bottling.refermentationMeterial)
                view!!.findViewById<EditText>(R.id.et_referm_amout).setText(validator.toStringOrEmpty(it.bottling.amountOfMaterial))
                view!!.findViewById<EditText>(R.id.et_but330).setText(validator.toStringOrEmpty(it.bottling.but033))
                view!!.findViewById<EditText>(R.id.et_but500).setText(validator.toStringOrEmpty(it.bottling.but05))
                bottlingDate = it.bottling.bottlingDate
                view!!.findViewById<TextView>(R.id.tv_bottling_date).text = validator.longDateToString(it.bottling.bottlingDate)
            }
        })
    }
}
