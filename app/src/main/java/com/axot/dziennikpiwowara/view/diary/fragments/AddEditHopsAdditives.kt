package com.axot.dziennikpiwowara.view.diary.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Additive
import com.axot.dziennikpiwowara.database.entitis.Hop
import com.axot.dziennikpiwowara.utils.SwipeToDeleteCallback
import com.axot.dziennikpiwowara.view.diary.adapters.AdditivesRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.ChargeRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.HopsRecyclerViewAdapter
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel


class AddEditHopsAdditives : Fragment() {

    var currentBeerId: Long? = null

    private lateinit var beersViewModel: BeersViewModel
    private lateinit var hopsAdapter: HopsRecyclerViewAdapter
    private lateinit var additivesAdapter: AdditivesRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add_edit_hops_additives, container, false)
        hopsAdapter = HopsRecyclerViewAdapter(activity!!)
        val hopsRecyclerView = view.findViewById<RecyclerView>(R.id.rv_hops)
        hopsRecyclerView.adapter = hopsAdapter
        hopsRecyclerView.layoutManager = LinearLayoutManager(activity!!)

        additivesAdapter = AdditivesRecyclerViewAdapter(activity!!)
        val additivesRecyclerView = view.findViewById<RecyclerView>(R.id.rv_additives)
        additivesRecyclerView.adapter = additivesAdapter
        additivesRecyclerView.layoutManager = LinearLayoutManager(activity!!)

        val hopsSwipeHandler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val adapter = hopsRecyclerView.adapter as HopsRecyclerViewAdapter
                beersViewModel.deleteHop(adapter.getHopAt(viewHolder!!.adapterPosition))
            }
        }
        val hopsItemTouchHelper = ItemTouchHelper(hopsSwipeHandler)
        hopsItemTouchHelper.attachToRecyclerView(hopsRecyclerView)

        val additivesSwipeHandler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val adapter = additivesRecyclerView.adapter as AdditivesRecyclerViewAdapter
                beersViewModel.deleteAdditive(adapter.getAdditiveAt(viewHolder!!.adapterPosition))
            }
        }
        val additivesItemTouchHelper = ItemTouchHelper(additivesSwipeHandler)
        additivesItemTouchHelper.attachToRecyclerView(additivesRecyclerView)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)

        beersViewModel.getAllHops(currentBeerId!!).observe(this, Observer { hops ->
            hops?.let {
                hopsAdapter.setHops(it)
            }
        })

        beersViewModel.getAllAdditives(currentBeerId!!).observe(this, Observer { additives ->
            additives?.let {
                additivesAdapter.setAdditives(it)
            }
        })

        view!!.findViewById<FloatingActionButton>(R.id.fab_hops_additives_add).setOnClickListener {
            fragmentManager!!.popBackStackImmediate()
        }

        view!!.findViewById<Button>(R.id.but_hop_add).setOnClickListener {
            addHop()
        }

        view!!.findViewById<Button>(R.id.but_additive_add).setOnClickListener {
            addAdditive()
        }
    }

    fun addHop() {
        val name: String = view!!.findViewById<EditText>(R.id.et_hop_name).text.toString()
        val alfa: String = view!!.findViewById<EditText>(R.id.et_alfa).text.toString()
        val time: String = view!!.findViewById<EditText>(R.id.et_hop_time).text.toString()
        val amount: String = view!!.findViewById<EditText>(R.id.et_hop_amount).text.toString()
        if(name == "" || alfa == "" || time == "" || amount == "") {
            Toast.makeText(activity!!, "Wypełnij wszystkie pola przed dodaniem", Toast.LENGTH_SHORT).show()
        } else {
            val hop = Hop(name = name, alfa = alfa.toDouble(), time = time.toInt(), amount = amount.toInt(), beerId = currentBeerId!!)
            beersViewModel.insertHop(hop)
            view!!.findViewById<EditText>(R.id.et_hop_name).setText("")
            view!!.findViewById<EditText>(R.id.et_alfa).setText("")
            view!!.findViewById<EditText>(R.id.et_hop_time).setText("")
            view!!.findViewById<EditText>(R.id.et_hop_amount).setText("")
        }
    }
    fun addAdditive() {
        val name: String = view!!.findViewById<EditText>(R.id.et_additive_name).text.toString()
        val amount: String = view!!.findViewById<EditText>(R.id.et_additive_amount).text.toString()
        val time: String = view!!.findViewById<EditText>(R.id.et_additive_time).text.toString()
        if(name == "" || amount == "" || time == "") {
            Toast.makeText(activity!!, "Wypełnij wszystkie pola przed dodaniem", Toast.LENGTH_SHORT).show()
        } else {
            val additive = Additive(name = name, amount = amount.toInt(), time = time.toInt(), beerId = currentBeerId!!)
            beersViewModel.insertAdditive(additive)
            view!!.findViewById<EditText>(R.id.et_additive_name).setText("")
            view!!.findViewById<EditText>(R.id.et_additive_amount).setText("")
            view!!.findViewById<EditText>(R.id.et_additive_time).setText("")
        }
    }


}
