package com.axot.dziennikpiwowara.view.diary.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Additive
import com.axot.dziennikpiwowara.database.entitis.FermAdditive
import com.axot.dziennikpiwowara.database.entitis.Hop
import com.axot.dziennikpiwowara.utils.SwipeToDeleteCallback
import com.axot.dziennikpiwowara.view.diary.adapters.AdditivesRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.FermAdditivesRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.HopsRecyclerViewAdapter
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel


class AddEditFermAdditives : Fragment() {

    var currentBeerId: Long? = null

    private lateinit var beersViewModel: BeersViewModel
    private lateinit var fermAdditivesAdapter: FermAdditivesRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add_edit_ferm_additives, container, false)
        fermAdditivesAdapter = FermAdditivesRecyclerViewAdapter(activity!!)
        val fermAdditivesRecyclerView = view.findViewById<RecyclerView>(R.id.rv_ferm_additives)
        fermAdditivesRecyclerView.adapter = fermAdditivesAdapter
        fermAdditivesRecyclerView.layoutManager = LinearLayoutManager(activity!!)

        val swipeHendler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val adapter = fermAdditivesRecyclerView.adapter as FermAdditivesRecyclerViewAdapter
                beersViewModel.deleteFermAdditive(adapter.getFermAdditivesAt(viewHolder!!.adapterPosition))
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHendler)
        itemTouchHelper.attachToRecyclerView(fermAdditivesRecyclerView)
        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)

        beersViewModel.getAllFermAdditives(currentBeerId!!).observe(this, Observer { fermAdditives ->
            fermAdditives?.let {
                fermAdditivesAdapter.setFermAdditives(it)
            }
        })

        view!!.findViewById<FloatingActionButton>(R.id.fab_ferm_additives_add).setOnClickListener {
            fragmentManager!!.popBackStackImmediate()
        }


        view!!.findViewById<Button>(R.id.but_ferm_additives_add).setOnClickListener {
            addFermAdditive()
        }


    }

    fun addFermAdditive() {
        val name: String = view!!.findViewById<EditText>(R.id.et_ferm_additive_name).text.toString()
        val amount: String = view!!.findViewById<EditText>(R.id.et_ferm_additive_amount).text.toString()
        val time: String = view!!.findViewById<EditText>(R.id.et_ferm_additive_time).text.toString()
        if (name == "" || amount == "" || time == "") {
            Toast.makeText(activity!!, "Wypełnij wszystkie pola przed dodaniem", Toast.LENGTH_SHORT).show()
        } else {
            val fermAdditive = FermAdditive(beerId = currentBeerId!!, name = name, amount = amount.toInt(), time = time.toInt())
            beersViewModel.insertFermAdditive(fermAdditive)
            view!!.findViewById<EditText>(R.id.et_ferm_additive_name).setText("")
            view!!.findViewById<EditText>(R.id.et_ferm_additive_amount).setText("")
            view!!.findViewById<EditText>(R.id.et_ferm_additive_time).setText("")
        }
    }


}
