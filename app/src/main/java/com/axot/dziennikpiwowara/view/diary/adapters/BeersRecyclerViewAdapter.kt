package com.axot.dziennikpiwowara.view.diary.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.BeerMinimal

class BeersRecyclerViewAdapter(context: Context) :
    RecyclerView.Adapter<BeersRecyclerViewAdapter.BeerViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var beers = emptyList<BeerMinimal>()
    var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val itemView = inflater.inflate(R.layout.beer_recycler_view_item, parent, false)
        return BeerViewHolder(itemView)
    }

    override fun getItemCount(): Int = beers.size

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        val current = beers[position]
        holder.name.text = current.name
        holder.style.text = current.style
        holder.blg.text = current.blg.toString()
    }

    fun setBeers(beers: List<BeerMinimal>) {
        this.beers = beers
        notifyDataSetChanged()

    }

    inner class BeerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.tvName)!!
        val style = itemView.findViewById<TextView>(R.id.tvStyle)!!
        val blg = itemView.findViewById<TextView>(R.id.tvBlg)!!

        init {
            itemView.setOnClickListener {
                if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
                    listener!!.onItemClick(beers[adapterPosition])
                }
            }
        }


    }

    interface OnItemClickListener {
        fun onItemClick(beer: BeerMinimal)
    }
}