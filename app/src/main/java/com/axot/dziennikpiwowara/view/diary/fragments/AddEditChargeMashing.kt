package com.axot.dziennikpiwowara.view.diary.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.axot.dziennikpiwowara.R
import com.axot.dziennikpiwowara.database.entitis.Charge
import com.axot.dziennikpiwowara.database.entitis.Mashing
import com.axot.dziennikpiwowara.utils.SwipeToDeleteCallback
import com.axot.dziennikpiwowara.view.diary.adapters.ChargeRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.FermAdditivesRecyclerViewAdapter
import com.axot.dziennikpiwowara.view.diary.adapters.MashingRecyclerViewAdapter
import com.axot.dziennikpiwowara.viewmodel.BeersViewModel


class AddEditChargeMashing : Fragment() {

    var currentBeerId: Long? = null
    private lateinit var beersViewModel: BeersViewModel
    private lateinit var chargeAdapter: ChargeRecyclerViewAdapter
    private lateinit var mashingAdapter: MashingRecyclerViewAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_add_edit_charge_mashing, container, false)
        chargeAdapter = ChargeRecyclerViewAdapter(activity!!)
        val rvCharge = view.findViewById<RecyclerView>(R.id.rv_charge)
        rvCharge.adapter = chargeAdapter
        rvCharge.layoutManager = LinearLayoutManager(activity!!)

        mashingAdapter = MashingRecyclerViewAdapter(activity!!)
        val rvMashing = view.findViewById<RecyclerView>(R.id.rv_mashing)
        rvMashing.adapter = mashingAdapter
        rvMashing.layoutManager = LinearLayoutManager(activity!!)

        val chargeSwipeHandler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val adapter = rvCharge.adapter as ChargeRecyclerViewAdapter
                beersViewModel.deleteCharge(adapter.getChargeAt(viewHolder!!.adapterPosition))
            }
        }
        val chargeItemTouchHelper = ItemTouchHelper(chargeSwipeHandler)
        chargeItemTouchHelper.attachToRecyclerView(rvCharge)

        val mashingSwipeHendler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val adapter = rvMashing.adapter as MashingRecyclerViewAdapter
                beersViewModel.deleteMashing(adapter.getMashingAt(viewHolder!!.adapterPosition))
            }
        }
        val maschingItemTouchHelper = ItemTouchHelper(mashingSwipeHendler)
        maschingItemTouchHelper.attachToRecyclerView(rvMashing)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        beersViewModel = ViewModelProviders.of(activity!!).get(BeersViewModel::class.java)
        beersViewModel.getAllCharge(currentBeerId!!).observe(this, Observer { charge ->
            charge?.let {
                chargeAdapter.setCharge(it)
            }
        })
        beersViewModel.getAllMashing(currentBeerId!!).observe(this, Observer {mashing ->
            mashing?.let {
                mashingAdapter.setMashing(it)
            }
        })

        view!!.findViewById<FloatingActionButton>(R.id.fab_charge_mashing_add).setOnClickListener {
            fragmentManager!!.popBackStackImmediate()
        }

        view!!.findViewById<Button>(R.id.but_charge_add).setOnClickListener {
            addCharge()
        }

        view!!.findViewById<Button>(R.id.but_mashing_add).setOnClickListener {
            addMashing()
        }
    }

    fun addCharge() {
        val chargeName: String = view!!.findViewById<EditText>(R.id.et_charge_name).text.toString()
        val chargeAmount: String = view!!.findViewById<EditText>(R.id.et_charge_amount).text.toString()
        if(chargeName == "" || chargeAmount == "") {
            Toast.makeText(activity!!, "Wypełnij wszystkie pola przed dodaniem", Toast.LENGTH_SHORT).show()
        } else {
            beersViewModel.insertCharge(Charge(beerId = currentBeerId!!, malt = chargeName, amountOfMalt = chargeAmount.toDouble()))
            view!!.findViewById<EditText>(R.id.et_charge_name).setText("")
            view!!.findViewById<EditText>(R.id.et_charge_amount).setText("")
        }
    }

    fun addMashing() {
        val mashingTemp: String = view!!.findViewById<EditText>(R.id.et_mashing_temperature).text.toString()
        val mashingTime: String = view!!.findViewById<EditText>(R.id.et_mashing_time).text.toString()

        if(mashingTemp == "" || mashingTime == "") {
            Toast.makeText(activity!!.applicationContext, "Wypełnij wszystkie pola przed dodaniem", Toast.LENGTH_SHORT).show()
        } else {
            beersViewModel.insertMashing(Mashing(beerId = currentBeerId!!,
                temperature = mashingTemp.toInt(), time = mashingTime.toInt()))
            view!!.findViewById<EditText>(R.id.et_mashing_temperature).setText("")
            view!!.findViewById<EditText>(R.id.et_mashing_time).setText("")
        }
    }

}
