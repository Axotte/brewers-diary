package com.axot.dziennikpiwowara.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.axot.dziennikpiwowara.database.BeersDatabase
import com.axot.dziennikpiwowara.database.entitis.*
import com.axot.dziennikpiwowara.utils.Repository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class BeersViewModel(application: Application): AndroidViewModel(application) {

    companion object {
        var currentBeerId: MutableLiveData<Long> = MutableLiveData()
    }

    private val repository: Repository

    //BeersDao
    val allBeers: LiveData<List<Beer>>
    val allBeersMinimal: LiveData<List<BeerMinimal>>
    val beersToDelete: LiveData<List<Beer>>

    init {
        val database = BeersDatabase.getDatabase(application)
        val beersDao = database.beersDao()
        val additivesDao = database.additivesDao()
        val chargeDao = database.chargeDao()
        val hopsDao = database.hopsDao()
        val mashingDao = database.mashingDao()
        val refermAdditivesDao = database.fermAdditivesDao()
        repository = Repository(beersDao, additivesDao, chargeDao, hopsDao, mashingDao, refermAdditivesDao)

        allBeers = repository.allBeers
        allBeersMinimal = repository.allBeersMinimal
        beersToDelete = repository.beersToDelete
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BeersDao

    fun getBeerDetalisById(id: Long): LiveData<BeerDetails> = repository.getBeerDetailsById(id)

    fun getBeerById(id: Long): LiveData<Beer> = repository.getBeerById(id)


    fun getId(): LiveData<Long> = currentBeerId

    fun insertBeer(beer: Beer) {
    doAsync {
        val beerID = repository.insertBeer(beer)
        uiThread {
            currentBeerId.value = beerID
        }
    }
}
    fun deleteAllBeers() {
        doAsync {
            repository.deleteAllBeers()
        }
    }

    fun deleteBeer(beer: Beer) {
        doAsync {
            repository.deleteBeer(beer)
        }
    }

    fun updateBeer(beer: Beer) {
        doAsync {
            repository.updateBeer(beer)
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //AdditivesDao

    fun getAllAdditives(beerId: Long): LiveData<List<Additive>> = repository.getAllAdditives(beerId)

    fun getAdditiveById(id: Long): LiveData<Additive> = repository.getAdditiveById(id)

    fun insertAdditive(additive: Additive) {
        doAsync {
            repository.insertAdditive(additive)
        }
    }

    fun updateAdditive(additive: Additive) {
        doAsync {
            repository.updateAdditive(additive)
        }
    }

    fun deleteAdditive(additive: Additive) {
        doAsync {
            repository.deleteAdditive(additive)
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ChargeDao

    fun getAllCharge(beerId: Long): LiveData<List<Charge>> = repository.getAllCharge(beerId)

    fun getChargeById(id: Long): LiveData<Charge> = repository.getChargeById(id)

    fun insertCharge(charge: Charge) {
        doAsync {
            repository.insertCharge(charge)
        }
    }

    fun updateCharge(charge: Charge) {
        doAsync {
            repository.updateCharge(charge)
        }
    }

    fun deleteCharge(charge: Charge) {
        doAsync {
            repository.deleteCharge(charge)
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //HopsDao

    fun getAllHops(beerId: Long): LiveData<List<Hop>> = repository.getAllHops(beerId)

    fun getHopById(id: Long): LiveData<Hop> = repository.getHopById(id)

    fun insertHop(hop: Hop) {
        doAsync {
            repository.insertHop(hop)
        }
    }

    fun updateHop(hop: Hop) {
        doAsync {
            repository.updateHop(hop)
        }
    }

    fun deleteHop(hop: Hop) {
        doAsync {
            repository.deleteHop(hop)
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MashingDao

    fun  getAllMashing(beerId: Long): LiveData<List<Mashing>> = repository.getAllMashing(beerId)

    fun getMashingById(id: Long): LiveData<Mashing> = repository.getMashingById(id)

    fun insertMashing(mashing: Mashing) {
        doAsync {
            repository.insertMashing(mashing)
        }
    }

    fun updateMashing(mashing: Mashing) {
        doAsync {
            repository.updateMashing(mashing)
        }
    }

    fun deleteMashing(mashing: Mashing) {
        doAsync {
            repository.deleteMashing(mashing)
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FermAdditivesDao

    fun getAllFermAdditives(beerId: Long): LiveData<List<FermAdditive>> = repository.getAllFermAdditives(beerId)

    fun getFermAdditiveById(id: Long): LiveData<FermAdditive> = repository.getFermAdditiveById(id)

    fun insertFermAdditive(fermAdditive: FermAdditive) {
        doAsync {
            repository.insertFermAdditive(fermAdditive)
        }
    }

    fun updateFermAdditive(fermAdditive: FermAdditive) {
        doAsync {
            repository.updateFermAdditive(fermAdditive)
        }
    }

    fun deleteFermAdditive(fermAdditive: FermAdditive) {
        doAsync {
            repository.deleteFermAdditive(fermAdditive)
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
}