package com.axot.dziennikpiwowara.utils

import java.util.*

class BeerValidator {

    fun toDoubleOrDefault(string: String): Double {
        return if(string.trim() == "") {
            0.0
        } else {
            string.toDouble()
        }
    }

    fun toIntOrDefault(string: String): Int {
        return if(string.trim() == "") {
            0
        } else {
            string.toInt()
        }
    }

    fun toStringOrEmpty(int: Int): String = if (int == 0) "" else int.toString()

    fun toStringOrEmpty(double: Double): String = if (double == 0.0) "" else double.toString()

    fun toTextView(string: String): String = if(string == "") "---" else string

    fun toTextView(int: Int): String = if(int == 0) "---" else int.toString()

    fun toTextView(double: Double): String = if(double == 0.0) "---" else double.toString()

    fun toTextViewBlg(double: Double): String = "${toTextView(double)} °Blg"

    fun toTextViewPercent(int: Int): String = "${toTextView(int)} %"

    fun toTextViewPercent(double: Double): String = "${toTextView(double)} %"

    fun toTextViewL(int: Int): String = "${toTextView(int)} l"

    fun toTextViewG(int: Int): String = "${toTextView(int)} g"

    fun toTextViewMinutes(int: Int): String = "${toTextView(int)} minut"

    fun toTextViewCelcius(int: Int): String = "${toTextView(int)} °C"

    fun dateToLong(year: Int, month: Int, day: Int): Long {
        val date = GregorianCalendar(year, month, day).time
        return date.time
    }

    fun longDateToString(milis: Long): String {

        return if(milis != 0.toLong()) {
            val date = Date(milis)
            val gregorianCalendar = GregorianCalendar()
            gregorianCalendar.time = date
            val day = gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH)
            val month = gregorianCalendar.get(GregorianCalendar.MONTH)
            val year = gregorianCalendar.get(GregorianCalendar.YEAR)
            "$day/$month/$year"
        } else {
            "--/--/----"
        }
    }
}