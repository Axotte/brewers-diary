package com.axot.dziennikpiwowara.utils

import android.arch.lifecycle.LiveData
import com.axot.dziennikpiwowara.database.daos.*
import com.axot.dziennikpiwowara.database.entitis.*

class Repository(
    private val beersDao: BeersDao,
    private val additivesDao: AdditivesDao,
    private val chargeDao: ChargeDao,
    private val hopsDao: HopsDao,
    private val mashingDao: MashingDao,
    private val fermAdditivesDao: FermAdditivesDao
) {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BeersDao

    val allBeers = beersDao.getAllBeers()
    val allBeersMinimal = beersDao.getAllBeersMinimal()
    val beersToDelete = beersDao.getBeersToDelete()

    fun getBeerDetailsById(id: Long): LiveData<BeerDetails> = beersDao.getBeerDetailsById(id)

    fun getBeerById(id: Long): LiveData<Beer> = beersDao.getBeerById(id)

    fun insertBeer(beer: Beer): Long {
        return beersDao.insert(beer)
    }

    fun deleteAllBeers(){
        beersDao.deleteAllBeers()
    }

    fun updateBeer(beer: Beer) {
        beersDao.update(beer)
    }

    fun deleteBeer(beer: Beer) {
        beersDao.delete(beer)
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //AdditivesDao

    fun getAllAdditives(beerId: Long): LiveData<List<Additive>> = additivesDao.getAllAdditives(beerId)

    fun getAdditiveById(id: Long): LiveData<Additive> = additivesDao.getAdditiveById(id)

    fun  insertAdditive(additive: Additive) {
        additivesDao.insert(additive)
    }

    fun  updateAdditive(additive: Additive) {
        additivesDao.update(additive)
    }

    fun  deleteAdditive(additive: Additive) {
        additivesDao.delete(additive)
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ChargeDao

    fun getAllCharge(beerId: Long): LiveData<List<Charge>> = chargeDao.getAllCharge(beerId)

    fun getChargeById(id: Long): LiveData<Charge> = chargeDao.getChargeById(id)

    fun insertCharge(charge: Charge) {
        chargeDao.insert(charge)
    }

    fun updateCharge(charge: Charge) {
        chargeDao.update(charge)
    }

    fun deleteCharge(charge: Charge) {
        chargeDao.delete(charge)
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //HopsDao

    fun getAllHops(beerId: Long): LiveData<List<Hop>> = hopsDao.getAllHops(beerId)

    fun getHopById(id: Long) = hopsDao.getHopById(id)

    fun insertHop(hop: Hop) {
        hopsDao.insert(hop)
    }

    fun updateHop(hop: Hop) {
        hopsDao.update(hop)
    }

    fun deleteHop(hop: Hop) {
        hopsDao.delete(hop)
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MashingDao

    fun getAllMashing(beerId: Long): LiveData<List<Mashing>> = mashingDao.getAllMashing(beerId)

    fun getMashingById(id: Long): LiveData<Mashing> = mashingDao.getMashingById(id)

    fun insertMashing(mashing: Mashing) {
        mashingDao.insert(mashing)
    }

    fun updateMashing(mashing: Mashing) {
        mashingDao.update(mashing)
    }

    fun deleteMashing(mashing: Mashing) {
        mashingDao.delete(mashing)
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FermAdditivesDao

    fun getAllFermAdditives(beerId: Long): LiveData<List<FermAdditive>> = fermAdditivesDao.getAllFermAdditives(beerId)

    fun getFermAdditiveById(id: Long): LiveData<FermAdditive> = fermAdditivesDao.getFermAdditiveById(id)

    fun insertFermAdditive(fermAdditive: FermAdditive) {
        fermAdditivesDao.insert(fermAdditive)
    }

    fun updateFermAdditive(fermAdditive: FermAdditive) {
        fermAdditivesDao.update(fermAdditive)
    }

    fun deleteFermAdditive(fermAdditive: FermAdditive) {
        fermAdditivesDao.delete(fermAdditive)
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

}